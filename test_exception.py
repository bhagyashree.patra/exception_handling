from exception import divide
import pytest

def test_divide():
    assert divide(10, 5) == 2

    with pytest.raises(ZeroDivisionError):
        assert 10/ 0 is None


    